package com.example.news.Dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.news.Activity.SearchActivity;
import com.example.news.R;

public class SearchDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.dialog_search, null);

        builder.setView(view)
                .setPositiveButton(R.string.search, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        TextView textView = (TextView) view.findViewById(R.id.search);
                        if (!textView.getText().toString().equals("")) {
                            Intent intent = new Intent(getContext(), SearchActivity.class);
                            intent.putExtra(SearchActivity.SEARCH, textView.getText().toString());
                            getActivity().startActivity(intent);
                        }
                    }
                });

        return builder.create();
    }
}
