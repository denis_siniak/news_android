package com.example.news.Dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.news.Model.Authentication;
import com.example.news.R;

public class SigninDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.dialog_signin, null);

        builder.setView(view)
                .setPositiveButton(R.string.signin, null)
                .setNegativeButton(R.string.register, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        DialogFragment dialogFragment = new RegisterDialog();
                        dialogFragment.show(getFragmentManager(), "register");
                    }
                });

        return builder.create();
    }

    @Override
    public void onResume() {
        super.onResume();
        final AlertDialog dialog = (AlertDialog) getDialog();
        if (dialog != null) {
            Button positiveButton = (Button) dialog.getButton(Dialog.BUTTON_POSITIVE);
            positiveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    TextView email = (TextView) dialog.findViewById(R.id.email);
                    TextView password = (TextView) dialog.findViewById(R.id.password);

                    if (email.getText().toString().equals("") || password.getText().toString().equals("")) {
                        Toast.makeText(getActivity(), " (」°ロ°)」 Заполните все поля!", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Authentication authentication = new Authentication();
                        authentication.signin(getActivity(), email.getText().toString(), password.getText().toString());
                    }
                }
            });
        }
    }
}
