package com.example.news.Activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.news.Entity.NewsTag;
import com.example.news.Fragment.ErrorFragment;
import com.example.news.Fragment.NewsRecycleViewFragment;
import com.example.news.Model.GetNews.GetNewsAdapter;
import com.example.news.Model.GetNews.GetNewsByTagAdapter;
import com.example.news.Model.GetNews.GetNewsFromBookmarks;
import com.example.news.R;
import com.google.firebase.auth.FirebaseAuth;

public class BookmarksActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookmarks);
        mAuth = FirebaseAuth.getInstance();


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (mAuth.getCurrentUser() == null){
            ErrorFragment errorFragment = new ErrorFragment();
            errorFragment.setError("Авторизуйтесь, чтобы использовать закладки");
            fragmentTransaction.replace(R.id.frameLayout, errorFragment);
        }
        else {
            NewsRecycleViewFragment newsRecycleViewFragment = new NewsRecycleViewFragment();
            GetNewsAdapter<String> getNewsAdapter = new GetNewsFromBookmarks();
            getNewsAdapter.setProperties(mAuth.getCurrentUser().getEmail());
//            newsRecycleViewFragment.setGetNewsAdapter(getNewsAdapter);
            fragmentTransaction.replace(R.id.frameLayout, newsRecycleViewFragment);
        }
        fragmentTransaction.commit();

        BottomNavigationView bottomNavigationView = (BottomNavigationView)findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setSelectedItemId(R.id.bookmarks);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home:{
                        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                        startActivity(intent);
                        break;
                    }
                    case R.id.categories: {
                        Intent intent = new Intent(getApplicationContext(), CategiriesActivity.class);
                        startActivity(intent);
                        break;
                    }
                    default:{
                        break;
                    }
                }
                return false;
            }
        });
    }
}
