package com.example.news.Activity;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.news.Fragment.NewsRecycleViewFragment;
import com.example.news.Model.GetNews.GetNewsAdapter;
import com.example.news.Model.GetNews.GetNewsBySearchAdapter;
import com.example.news.R;

public class SearchActivity extends AppCompatActivity {

    public static final String SEARCH = "search";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.arrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        Intent intent = getIntent();

        GetNewsAdapter<String> getNewsAdapter = new GetNewsBySearchAdapter();
        getNewsAdapter.setProperties(intent.getStringExtra(SEARCH));

        NewsRecycleViewFragment newsRecycleViewFragment = new NewsRecycleViewFragment();
//        newsRecycleViewFragment.setGetNewsAdapter(getNewsAdapter);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, newsRecycleViewFragment);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}
