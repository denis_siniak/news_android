package com.example.news.Activity;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.news.Adapter.ImagePageAdapter;
import com.example.news.DTO.BookmarkDTO;
import com.example.news.Entity.News;
import com.example.news.Model.RequestService;
import com.example.news.Model.GetNews.GetNewsAdapter;
import com.example.news.Model.GetNews.GetNewsById;
import com.example.news.Model.GetNews.MyAsyncTask;
import com.example.news.Model.NetworkService;
import com.example.news.R;
import com.google.firebase.auth.FirebaseAuth;


import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;

public class NewsActivity extends AppCompatActivity {

    private News news;
    public static final String NEWS_ID = "newsId";
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private Boolean isInBookmark;
    MenuItem addBookmark;
    MenuItem removeBookmark;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        createNews();
        createToolbar();

        ImagePageAdapter imagePageAdapter = new ImagePageAdapter(this, news.getImages());
        ViewPager viewPager = findViewById(R.id.imagesViewPager);
        viewPager.setAdapter(imagePageAdapter);
        CircleIndicator circleIndicator = findViewById(R.id.circleIndicator);
        circleIndicator.setViewPager(viewPager);

        final TextView newsAuthor = (TextView) findViewById(R.id.newsAuthor);
        newsAuthor.setText(news.getAuthor().getName() + ", " + news.getAuthor().getPosition());
        newsAuthor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), AuthorActivity.class);
                intent.putExtra(AuthorActivity.AUTHOR_NAME, news.getAuthor().getEmail());
                startActivity(intent);
            }
        });

        TextView newsTitle = (TextView) findViewById(R.id.newsTitle);
        newsTitle.setText(news.getTitle());
        TextView newsDate = (TextView) findViewById(R.id.newsDate);
        newsDate.setText(news.getDate());
        TextView newsText = (TextView) findViewById(R.id.newsText);
        newsText.setText(news.getNews());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_bokmarks, menu);
        addBookmark = menu.findItem(R.id.addBookmark);
        removeBookmark = menu.findItem(R.id.removeBookmark);

        if (mAuth.getCurrentUser() != null){
            addBookmark.setVisible(true);
            removeBookmark.setVisible(false);

            if (isInBookmark)
                changeVisible();
        }

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addBookmark: {
                    Call<Boolean> call = NetworkService.getInstance().getJSONApi().addToBookmarks(new BookmarkDTO(mAuth.getCurrentUser().getEmail(), news.getId()));
                    bookmark(call);
                    break;
            }
            case R.id.removeBookmark: {
                Call<Boolean> call = NetworkService.getInstance().getJSONApi().removeFromBookmarks(new BookmarkDTO(mAuth.getCurrentUser().getEmail(), news.getId()));
                bookmark(call);
                break;
            }
            default: {
                onBackPressed();
                break;
            }

        }
        return super.onOptionsItemSelected(item);
    }

    private void createNews() {
        Intent intent = getIntent();
        GetNewsAdapter<Integer> getNewsAdapter = new GetNewsById();
        getNewsAdapter.setProperties(intent.getIntExtra(NEWS_ID, 0));
        MyAsyncTask myAsyncTask = new MyAsyncTask();
        myAsyncTask.setGetNewsAdapter(this, getNewsAdapter);
        myAsyncTask.execute();

        while (getNewsAdapter.getNews() == null || getNewsAdapter.getNews().isEmpty()) {

        }

        news = getNewsAdapter.getNews().get(0);
    }

    private void createToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.arrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        if (mAuth.getCurrentUser() != null) {
            Call<Boolean> call = NetworkService.getInstance().getJSONApi().isInBookmark(new BookmarkDTO(mAuth.getCurrentUser().getEmail(), news.getId()));
            RequestService requestService = new RequestService();
            requestService.setCall(call);
            requestService.execute();

            while (requestService.getResult() == null) {
            }

            isInBookmark = requestService.getResult();
        } else
            isInBookmark = false;
    }

    private void bookmark(Call<Boolean> call) {
        RequestService requestService = new RequestService();
        requestService.setCall(call);
        requestService.execute();

        while (requestService.getResult() == null){}

        if (requestService.getResult()) {
            Toast.makeText(getApplicationContext(), "Успех!", Toast.LENGTH_SHORT).show();
            changeVisible();
        } else
            Toast.makeText(getApplicationContext(), "Упс, не получилось", Toast.LENGTH_SHORT).show();
    }

    private void changeVisible(){
        addBookmark.setVisible(!addBookmark.isVisible());
        removeBookmark.setVisible(!removeBookmark.isVisible());
    }

}
