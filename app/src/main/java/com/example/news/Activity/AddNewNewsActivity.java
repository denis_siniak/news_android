package com.example.news.Activity;

import android.Manifest;
import android.content.ClipData;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.news.DTO.NewsDTO;
import com.example.news.Entity.NewsTag;
import com.example.news.Model.NetworkService;
import com.example.news.Model.RequestService;
import com.example.news.R;
import com.google.firebase.auth.FirebaseAuth;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

public class AddNewNewsActivity extends AppCompatActivity {

    private static final int GALLERY_REQUEST_CODE = 0;
    private static final int PERMISSION_REQUEST_CODE = 1;

    private FirebaseAuth mAuth;
    private String title;
    private String newsText;
    private Integer newsTagId;

    EditText editTitle;
    EditText editText;

    List<String> imagesPath = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_news);
        mAuth = FirebaseAuth.getInstance();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        List<String> data = new ArrayList<>();
        data.add("Тег новости");
        for (int i = 1; i < NewsTag.values().length; i++) {
            data.add(NewsTag.values()[i].getName());
        }

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner spinner = (Spinner) findViewById(R.id.newsTag);
        spinner.setAdapter(adapter);
        spinner.setPrompt("Выберите тег:");
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                newsTagId = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        editTitle = (EditText) findViewById(R.id.newsTitle);
        editText = (EditText) findViewById(R.id.newsText);

        Button gallery = (Button) findViewById(R.id.gallery);

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickFromGallery();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.apply){
            done();
        }

        return true;
    }

    private void done(){

        title = editTitle.getText().toString();
        newsText = editText.getText().toString();

        if (newsTagId == 0 || title.equals("") || newsText.equals("")) {
            Toast.makeText(getApplicationContext(), "Заполните все поля", Toast.LENGTH_SHORT).show();
            return;
        }

        List<MultipartBody.Part> parts = new ArrayList<>();
        for (String path : imagesPath) {
            File file = new File(path);
            RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part part = MultipartBody.Part.createFormData("images", file.getName(), fileReqBody);
            parts.add(part);
        }

        NewsDTO newsDTO = new NewsDTO();
        newsDTO.setTitle(title);
        newsDTO.setNews(newsText);
        newsDTO.setAuthorEmail(mAuth.getCurrentUser().getEmail());
        newsDTO.setNewsTagId(newsTagId);

        Call<Boolean> call = NetworkService.getInstance().getJSONApi().addNewNews(newsDTO, parts);
        RequestService requestService = new RequestService();
        requestService.setCall(call);
        requestService.execute();

        while (requestService.getResult() == null) {
        }

        if (requestService.getResult()) {
            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
            startActivity(intent);
            Toast.makeText(getApplicationContext(), "Новость добавлена", Toast.LENGTH_SHORT).show();
        } else
            Toast.makeText(getApplicationContext(), "Ошибка", Toast.LENGTH_SHORT).show();
    }

    private void pickFromGallery() {
        requestMultiplePermissions();
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        String[] mimeTypes = {"image/jpeg", "image/png"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK) {
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                if (data.getData() != null) {
                    Uri mImageUri = data.getData();
                    Cursor cursor = getContentResolver().query(mImageUri, filePathColumn, null, null, null);
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    imagesPath.add(cursor.getString(columnIndex));
                    cursor.close();
                } else {
                    if (data.getClipData() != null) {
                        ClipData mClipData = data.getClipData();
                        ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                        for (int i = 0; i < mClipData.getItemCount(); i++) {
                            ClipData.Item item = mClipData.getItemAt(i);
                            Uri uri = item.getUri();
                            mArrayUri.add(uri);
                            Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
                            cursor.moveToFirst();
                            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                            imagesPath.add(cursor.getString(columnIndex));
                            cursor.close();
                        }
                    }
                }
                Toast.makeText(getApplicationContext(), "Вы добывили фото: " + imagesPath.size() + " шт" , Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "Вы не выбрали ни одну фотографию", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Что-то пошло не так", Toast.LENGTH_LONG).show();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    private void requestMultiplePermissions() {
        ActivityCompat.requestPermissions(this,
                new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                },
                PERMISSION_REQUEST_CODE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.apply_toolbar, menu);
        return true;
    }
}
