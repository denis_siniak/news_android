package com.example.news.Activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.example.news.Dialog.SearchDialog;
import com.example.news.Dialog.SigninDialog;
import com.example.news.Model.Authentication;
import com.example.news.Model.NetworkService;
import com.example.news.Model.RequestService;
import com.example.news.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import retrofit2.Call;


public class HomeActivity extends AppCompatActivity{

    private FirebaseAuth mAuth;
    FirebaseUser firebaseUser;
    private Boolean isAuthor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        mAuth = FirebaseAuth.getInstance();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        if (mAuth.getCurrentUser() != null){
            Call<Boolean> call = NetworkService.getInstance().getJSONApi().isAuthor(mAuth.getCurrentUser().getEmail());
            RequestService requestService = new RequestService();
            requestService.setCall(call);
            requestService.execute();

            while (requestService.getResult() == null){}

            isAuthor = requestService.getResult();
        }
        else
            isAuthor = false;

        BottomNavigationView bottomNavigationView = (BottomNavigationView)findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setSelectedItemId(R.id.home);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.bookmarks:{
                        Intent intent = new Intent(getApplicationContext(), BookmarksActivity.class);
                        startActivity(intent);
                        break;
                    }
                    case R.id.categories: {
                        Intent intent = new Intent(getApplicationContext(), CategiriesActivity.class);
                        startActivity(intent);
                        break;
                    }
                    default:{
                        break;
                    }
                }
                return false;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_toolbar, menu);
        MenuItem signin = menu.findItem(R.id.signin_icon);
        MenuItem logout = menu.findItem(R.id.logout_icon);
        if (firebaseUser == null){
            signin.setVisible(true);
            logout.setVisible(false);
        }
        else {
            signin.setVisible(false);
            logout.setVisible(true);
        }

        if (isAuthor){
            MenuItem addNews = menu.findItem(R.id.add_news);
            addNews.setVisible(true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.add_news:{
                Intent intent = new Intent(getApplicationContext(), AddNewNewsActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.signin_icon: {
                DialogFragment dialogFragment = new SigninDialog();
                dialogFragment.show(getSupportFragmentManager(), "signin");
                break;
            }
            case R.id.search_icon:{
                DialogFragment dialogFragment = new SearchDialog();
                dialogFragment.show(getSupportFragmentManager(), "search");
                break;
            }
            case R.id.logout_icon:{
                Authentication authentication = new Authentication();
                authentication.logout(this);
                break;
            }
        }
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
        firebaseUser = mAuth.getCurrentUser();
        Authentication.setFirebaseUser(firebaseUser);
    }
}
