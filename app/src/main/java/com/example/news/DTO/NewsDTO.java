package com.example.news.DTO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewsDTO {

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("news")
    @Expose
    private String news;

    @SerializedName("authorEmail")
    @Expose
    private String authorEmail;

    @SerializedName("newsTagId")
    @Expose
    private Integer newsTagId;

    public NewsDTO() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNews() {
        return news;
    }

    public void setNews(String news) {
        this.news = news;
    }

    public String getAuthorEmail() {
        return authorEmail;
    }

    public void setAuthorEmail(String authorEmail) {
        this.authorEmail = authorEmail;
    }

    public Integer getNewsTagId() {
        return newsTagId;
    }

    public void setNewsTagId(Integer newsTagId) {
        this.newsTagId = newsTagId;
    }
}
