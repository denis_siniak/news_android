package com.example.news.DTO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BookmarkDTO {

    @SerializedName("userEmail")
    @Expose
    private String userEmail;

    @SerializedName("newsId")
    @Expose
    private Integer newsId;

    public BookmarkDTO() {
    }

    public BookmarkDTO(String userEmail, Integer newsId) {
        this.userEmail = userEmail;
        this.newsId = newsId;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public Integer getNewsId() {
        return newsId;
    }

    public void setNewsId(Integer newsId) {
        this.newsId = newsId;
    }
}
