package com.example.news.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.news.Entity.News;
import com.example.news.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private LayoutInflater inflater;
    private List<News> news;
    private OnClickListener listener;

    public RecyclerViewAdapter(Context context, List<News> news) {
        this.inflater = LayoutInflater.from(context);
        this.news = news;
    }

    public void setListener(OnClickListener listener){
        this.listener = listener;
    }

    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = inflater.inflate(R.layout.news_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewAdapter.ViewHolder holder, int position) {
        News singleNews = news.get(position);

        try {
            String url = "http://192.168.0.106:8080/image/get/" + news.get(position).getImages().get(0);
            Picasso.get()
                    .load(url)
                    .fit()
                    .error(R.drawable.basic_image)
                    .into(holder.newsImage);
        }
        catch (IndexOutOfBoundsException e){
            holder.newsImage.setImageResource(R.drawable.basic_image);
        }

        holder.newsTitle.setText(singleNews.getTitle());
        holder.click(news.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return news.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        final ImageView newsImage;
        final TextView newsTitle;

        ViewHolder(View view) {
            super(view);
            newsImage = (ImageView)view.findViewById(R.id.newsImage);
            newsTitle = (TextView)view.findViewById(R.id.newsTitle);
        }

        public void click(final News item, final OnClickListener listener){
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }

    public void refresh(List<News> list){
        news = new ArrayList<>();
        news.addAll(list);
        notifyDataSetChanged();
    }
}
