package com.example.news.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.news.Entity.NewsTag;
import com.example.news.Fragment.NewsRecycleViewFragment;
import com.example.news.Model.GetNews.GetNewsAdapter;
import com.example.news.Model.GetNews.GetNewsByTagAdapter;
import com.example.news.Model.GetNews.MyAsyncTask;

import java.util.List;

public class SectionsPageAdapter extends FragmentPagerAdapter {

    private List<NewsTag> newsTags ;

    public SectionsPageAdapter(FragmentManager fm, List<NewsTag> newsTags) {
        super(fm);
        this.newsTags = newsTags;
    }

    @Override
    public int getCount(){
        return newsTags.size();
    }

    @Override
    public Fragment getItem(int position){
        NewsRecycleViewFragment fragment = new NewsRecycleViewFragment();
        GetNewsAdapter<NewsTag> getNewsAdapter = new GetNewsByTagAdapter();
        getNewsAdapter.setProperties(newsTags.get(position));
//        fragment.setGetNewsAdapter(getNewsAdapter);
        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position){
        return newsTags.get(position).getName();
    }


}
