package com.example.news.Adapter;

import com.example.news.Entity.News;

public interface OnClickListener {
    void onItemClick(News item);
}
