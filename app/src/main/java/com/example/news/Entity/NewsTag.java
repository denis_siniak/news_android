package com.example.news.Entity;

public enum  NewsTag {

    ALL(0, "Новости"),
    TECHNOLOGIES(1, "Технологии"),
    GAMES(2, "Игры"),
    SCIENCE(3, "Наука"),
    FOOD(4, "Еда"),
    HEALTH(5, "Здоровье"),
    TRAVELING(6, "Путешествия"),
    SPORT(7, "Спорт"),
    AUTO(8, "Авто"),
    FASHION_AND_BEAUTY(9, "Мода и Красота"),
    LIFE_STYLE(10, "Стиль жизни"),
    PHOTO(11, "Фото"),
    CINEMA(12, "Кино"),
    MUSIC(13, "Музыка"),
    CULTURE(14, "Культура"),
    BOOKS(15, "Книги"),
    BUSINESS(16, "Бизнес"),
    MEDIA(17, "Медиа"),
    POLICY(18, "Политика"),
    OPINIONS(19, "Мнения"),
    SOCIETY(20, "Общество");


    private Integer id;
    private String name;

    NewsTag(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public static NewsTag findByName(String name) throws IllegalArgumentException{
        for (NewsTag newsTag: NewsTag.values()){
            if (name.equals(newsTag.name))
                return newsTag;
        }

        throw new IllegalArgumentException();
    }

    public static NewsTag findById(Integer id) throws  IllegalArgumentException{
        for (NewsTag newsTag: NewsTag.values()){
            if (id.equals(newsTag.id))
                return newsTag;
        }

        throw new IllegalArgumentException();
    }


    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
