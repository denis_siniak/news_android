package com.example.news.Model.GetNews;

import android.os.AsyncTask;

import com.example.news.Entity.News;
import com.example.news.Model.NetworkService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public class GetNewsByAuthorAdapter implements GetNewsAdapter<String> {

    private List<News> news;
    private String authorEmail;
    private String error;

    @Override
    public void setProperties(String authorName) {
        this.authorEmail = authorName;
    }

    @Override
    public void makeNews() {
        if (authorEmail == null)
            throw new NullPointerException("enter properties");

        NetworkService networkService = NetworkService.getInstance();
        Call<List<News>> call = networkService.getJSONApi().getNewsByAuthor(authorEmail);
        try {
            news = call.execute().body();
        }
        catch (IOException e){
            news = new ArrayList<>();
            error = "Упс, проблемы с сервером";
        }
    }

    @Override
    public List<News> getNews() {
        return news;
    }

    @Override
    public void resetNews() {
        news = null;
    }

    @Override
    public String getError() {
        return error;
    }
}
