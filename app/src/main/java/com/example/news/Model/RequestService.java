package com.example.news.Model;

import android.os.AsyncTask;

import java.io.IOException;

import retrofit2.Call;

public class RequestService extends AsyncTask<Void, Void, Void> {

    private Call<Boolean> call;
    private Boolean result;

    @Override
    protected Void doInBackground(Void... voids) {
        if (call == null)
            throw new NullPointerException("");

        try {
            result = call.execute().body();
        }
        catch (IOException e){
            e.printStackTrace();
            result = false;
        }

        return null;
    }

    public void setCall(Call call) {
        this.call = call;
    }

    public Boolean getResult() {
        return result;
    }
}
