package com.example.news.Model.GetNews;

import com.example.news.Entity.News;
import com.example.news.Model.JSONPlaceHolderApi;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;

public class NewsListService {

    public static final String URL = "http://192.168.0.106:8080";

    private static NewsListService instance;
    private Retrofit retrofit;

    private static Observable<List<News>> observableRetrofit;
    private static BehaviorSubject<List<News>> observableObjectsList;
    private static Subscription subscription;

    private NewsListService(){
        RxJavaCallAdapterFactory rxJavaCallAdapterFactory = RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io());

        Gson gson = new GsonBuilder().create();

        retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(rxJavaCallAdapterFactory)
                .build();
    }

    public static NewsListService getInstance(){
        if (instance == null){
            instance = new NewsListService();
        }

        return instance;
    }

    public JSONPlaceHolderApi getJSONApi(){
        return retrofit.create(JSONPlaceHolderApi.class);
    }

    public static void init(Observable<List<News>> observable){
        observableRetrofit = observable;
    }

    public static void resetObservable(){
        observableObjectsList = BehaviorSubject.create();

        if (subscription != null && !subscription.isUnsubscribed()){
            subscription.unsubscribe();
        }
        subscription = observableRetrofit.subscribe(new Subscriber<List<News>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                observableObjectsList.onError(e);
            }

            @Override
            public void onNext(List<News> news) {
                observableObjectsList.onNext(news);
            }
        });
    }

    public static Observable<List<News>> getObjectObservable(){
        if (observableObjectsList == null){
            resetObservable();
        }

        return observableObjectsList;
    }

    public static void setObservableRetrofit(Observable<List<News>> observableRetrofit) {
        NewsListService.observableRetrofit = observableRetrofit;
    }
}
