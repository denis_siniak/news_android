package com.example.news.Model.GetNews;

import android.os.AsyncTask;

import com.example.news.Entity.News;

import java.util.List;

public interface GetNewsAdapter<T> {

    void setProperties(T properties);

    void makeNews();

    List<News> getNews();

    void resetNews();

    String getError();
}
