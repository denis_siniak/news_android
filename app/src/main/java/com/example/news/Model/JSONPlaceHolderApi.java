package com.example.news.Model;


import com.example.news.DTO.BookmarkDTO;
import com.example.news.DTO.NewsDTO;
import com.example.news.Entity.News;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import rx.Observable;

public interface JSONPlaceHolderApi {

    @GET("/news/getByTag/{tag}")
    public Observable<List<News>> getNewsByTag(@Path("tag")String tag);

    @GET("/news/getByAuthor/{author}")
    public Call<List<News>> getNewsByAuthor(@Path("author")String author);

    @GET("/news/getBySearch/{search}")
    public Call<List<News>> getNewsBySearch(@Path("search")String search);

    @GET("/news/getBookmarks/{userEmail}")
    public Call<List<News>> getBookmarks(@Path("userEmail")String userEmail);

    @GET("/news/getById/{id}")
    public Call<News> getNewsById(@Path("id")Integer id);

    @POST("/bookmarks/isInBookmark")
    Call<Boolean> isInBookmark(@Body BookmarkDTO bookmark);

    @POST("/bookmarks/add")
    Call<Boolean> addToBookmarks(@Body BookmarkDTO bookmark);

    @POST("/bookmarks/remove")
    Call<Boolean> removeFromBookmarks(@Body BookmarkDTO bookmark);

    @GET("/author/isAuthor/{email}")
    public Call<Boolean> isAuthor(@Path("email")String email);

    @Multipart
    @POST("/author/add")
    public Call<Boolean> addNewNews(@Part("newsDTO") NewsDTO newsDTO, @Part List<MultipartBody.Part> images);
}
