package com.example.news.Model.GetNews;

import com.example.news.Entity.Author;
import com.example.news.Entity.News;
import com.example.news.Model.NetworkService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;

public class GetNewsById implements GetNewsAdapter<Integer> {

    private List<News> news;
    private Integer id;
    private String error;

    @Override
    public void setProperties(Integer id) {
        this.id = id;
    }

    @Override
    public void makeNews() {
        if (id == null)
            throw new NullPointerException("enter properties");

        NetworkService networkService = NetworkService.getInstance();
        Call<News> call = networkService.getJSONApi().getNewsById(id);
        try {
            News news = call.execute().body();
            this.news = new ArrayList<>();
            this.news.add(news);
        }
        catch (IOException e){
            news = new ArrayList<>();
            error = "Упс, проблемы с сервером";
        }
    }

    @Override
    public List<News> getNews() {
        return news;
    }

    @Override
    public void resetNews() {
        news = null;
    }

    @Override
    public String getError() {
        return error;
    }
}
