package com.example.news.Model.GetNews;

import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.example.news.R;

import dmax.dialog.SpotsDialog;

public class MyAsyncTask extends AsyncTask<Void, Void, Void> {

    private GetNewsAdapter getNewsAdapter;
    private Context context;
    private AlertDialog alertDialog;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        alertDialog = new SpotsDialog(context, R.style.Custom);
        alertDialog.show();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        getNewsAdapter.makeNews();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        alertDialog.dismiss();
    }

    public void setGetNewsAdapter(Context context, GetNewsAdapter getNewsAdapter) {
        this.getNewsAdapter = getNewsAdapter;
        this.context = context;
    }
}
