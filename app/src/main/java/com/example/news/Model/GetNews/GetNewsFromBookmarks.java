package com.example.news.Model.GetNews;

import com.example.news.Activity.BookmarksActivity;
import com.example.news.Entity.News;
import com.example.news.Model.NetworkService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public class GetNewsFromBookmarks implements GetNewsAdapter<String> {

    private List<News> news;
    private String userEmail;
    private String error;

    @Override
    public void setProperties(String userEmail) {
        this.userEmail = userEmail;
    }

    @Override
    public void makeNews() {
        if (userEmail == null)
            throw new NullPointerException("enter properties");

        NetworkService networkService = NetworkService.getInstance();
        Call<List<News>> call = networkService.getJSONApi().getBookmarks(userEmail);
        try {
            news = call.execute().body();
            if (news.isEmpty()){
                error = "У вас нет сохраненнх статей";
            }
        }
        catch (IOException e){
            news = new ArrayList<>();
            error = "Упс, проблемы с сервером";
        }
    }

    @Override
    public List<News> getNews() {
        return news;
    }

    @Override
    public void resetNews() {
        news = null;
    }

    @Override
    public String getError() {
        return error;
    }
}
