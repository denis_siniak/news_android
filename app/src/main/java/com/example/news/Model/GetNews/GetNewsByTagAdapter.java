package com.example.news.Model.GetNews;

import android.os.AsyncTask;
import android.widget.Toast;

import com.example.news.Entity.News;
import com.example.news.Entity.NewsTag;
import com.example.news.Model.NetworkService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public class GetNewsByTagAdapter implements GetNewsAdapter<NewsTag> {

    private List<News> news;
    private NewsTag newsTag;
    private String error;

    @Override
    public void setProperties(NewsTag newsTag) {
        this.newsTag = newsTag;
    }

    @Override
    public void makeNews() {
        if (newsTag == null)
            throw new NullPointerException("enter properties");

        NetworkService networkService = NetworkService.getInstance();
//        Call<List<News>> call = networkService.getJSONApi().getNewsByTag(newsTag.getName());
//        try {
//            news = call.execute().body();
//        }
//        catch (IOException e){
//            news = new ArrayList<>();
//            error = "Упс, проблемы с сервером";
//        }
    }

    @Override
    public List<News> getNews() {
        return news;
    }

    @Override
    public void resetNews() {
        news = null;
    }

    @Override
    public String getError() {
        return error;
    }
}
