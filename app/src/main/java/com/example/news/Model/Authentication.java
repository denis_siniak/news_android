package com.example.news.Model;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.example.news.Activity.HomeActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;

public class Authentication {

    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private static FirebaseUser firebaseUser;

    public void signin(final Activity activity, String email, String password){
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            firebaseUser = mAuth.getCurrentUser();
                            Intent intent = new Intent(activity, HomeActivity.class);
                            activity.startActivity(intent);
                            Toast.makeText(activity, " (づ ◕‿◕ )づ Здравствуйте!!!", Toast.LENGTH_LONG).show();
                        }
                    }
                })
                .addOnFailureListener(activity, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        if (e instanceof FirebaseAuthInvalidCredentialsException){
                            Toast.makeText(activity, " ¯\\_(ツ)_/¯ Не верный логин или пароль", Toast.LENGTH_SHORT).show();
                        }
                        e.printStackTrace();
                    }
                });
    }

    public void registration(final Activity activity, String email, String password){
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            firebaseUser = mAuth.getCurrentUser();
                            Intent intent = new Intent(activity, HomeActivity.class);
                            Toast.makeText(activity, " (づ￣ ³￣)づ Вы зарегистрированы", Toast.LENGTH_LONG).show();
                            activity.startActivity(intent);
                        }
                    }
                })
                .addOnFailureListener(activity, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        if (e.getMessage().equals("The email address is badly formatted."))
                            Toast.makeText(activity, " <(￣ ﹌ ￣)> Не верный формат Email", Toast.LENGTH_SHORT).show();
                        if (e.getMessage().equals("The given password is invalid. [ Password should be at least 6 characters ]"))
                            Toast.makeText(activity, " <(￣ ﹌ ￣)> Плохой пароль (минимум 6 символов)", Toast.LENGTH_SHORT).show();
                        if (e.getMessage().equals("The email address is already in use by another account."))
                            Toast.makeText(activity, "Такой email уже зарегистрирован", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                });
    }

    public void logout(Activity activity){
        mAuth.signOut();
        Intent intent = new Intent(activity, HomeActivity.class);
        activity.startActivity(intent);
    }

    public static FirebaseUser getFirebaseUser() {
        return firebaseUser;
    }

    public static void setFirebaseUser(FirebaseUser firebaseUser) {
        Authentication.firebaseUser = firebaseUser;
    }
}
