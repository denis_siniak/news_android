package com.example.news.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.example.news.Activity.HomeActivity;
import com.example.news.DataBase.DataBaseHelper;
import com.example.news.Entity.NewsTag;
import com.example.news.R;
import com.xeoh.android.checkboxgroup.CheckBoxGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class CheckListFragment extends Fragment {

    private HashMap<CheckBox, String> checkBoxMap = new HashMap<>();
    private List<NewsTag> newsTags;
    private Set<NewsTag> newNewsTags;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_check_list, container, false);

        createMap(view);
        setNewsTags();
        checkBoxes();

        CheckBoxGroup<String> checkBoxGroup = new CheckBoxGroup<>(checkBoxMap, new CheckBoxGroup.CheckedChangeListener<String>() {
            @Override
            public void onCheckedChange(ArrayList<String> values) {
                newNewsTags = new LinkedHashSet<>();
                for (String value: values){
                    newNewsTags.add(NewsTag.findByName(value));
                }
            }
        });

        return view;
    }

    public void done(){
        if (newNewsTags != null) {
            DataBaseHelper dataBaseHelper = new DataBaseHelper(getContext());
            dataBaseHelper.insertTags(newNewsTags);
        }
        Intent intent = new Intent(getContext(), HomeActivity.class);
        startActivity(intent);
    }

    private void createMap(View view){
        checkBoxMap.put( (CheckBox) view.findViewById(R.id.technologies), "Технологии");
        checkBoxMap.put( (CheckBox) view.findViewById(R.id.games), "Игры");
        checkBoxMap.put((CheckBox) view.findViewById(R.id.science), "Наука");
        checkBoxMap.put((CheckBox) view.findViewById(R.id.food), "Еда");
        checkBoxMap.put((CheckBox) view.findViewById(R.id.health), "Здоровье");
        checkBoxMap.put((CheckBox) view.findViewById(R.id.traveling), "Путешествия");
        checkBoxMap.put((CheckBox) view.findViewById(R.id.sport), "Спорт");
        checkBoxMap.put((CheckBox) view.findViewById(R.id.auto), "Авто");
        checkBoxMap.put((CheckBox) view.findViewById(R.id.fashionAndBeauty), "Мода и Красота");
        checkBoxMap.put((CheckBox) view.findViewById(R.id.lifeStyle), "Стиль жизни");
        checkBoxMap.put((CheckBox) view.findViewById(R.id.photo), "Фото");
        checkBoxMap.put((CheckBox) view.findViewById(R.id.cinema), "Кино");
        checkBoxMap.put((CheckBox) view.findViewById(R.id.music), "Музыка");
        checkBoxMap.put((CheckBox) view.findViewById(R.id.culture), "Культура");
        checkBoxMap.put((CheckBox) view.findViewById(R.id.books), "Книги");
        checkBoxMap.put((CheckBox) view.findViewById(R.id.business), "Бизнес");
        checkBoxMap.put((CheckBox) view.findViewById(R.id.media), "Медиа");
        checkBoxMap.put((CheckBox) view.findViewById(R.id.policy), "Политика");
        checkBoxMap.put((CheckBox) view.findViewById(R.id.opinions), "Мнения");
        checkBoxMap.put((CheckBox) view.findViewById(R.id.society), "Общество");
    }

    private void setNewsTags(){
        DataBaseHelper dataBaseHelper = new DataBaseHelper(getContext());
        newsTags = dataBaseHelper.pullTags();
    }

    private void checkBoxes(){
        for (NewsTag tag: newsTags){
            if (checkBoxMap.containsValue(tag.getName())){
                for (CheckBox checkBox: checkBoxMap.keySet())
                    if (checkBoxMap.get(checkBox).equals(tag.getName()))
                        checkBox.setChecked(true);
            }
        }
    }
}
