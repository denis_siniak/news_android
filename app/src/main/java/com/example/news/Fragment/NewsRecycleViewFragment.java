package com.example.news.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.news.Activity.NewsActivity;
import com.example.news.Adapter.OnClickListener;
import com.example.news.Adapter.RecyclerViewAdapter;
import com.example.news.Entity.News;
import com.example.news.Model.GetNews.NewsListService;
import com.example.news.R;

import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class NewsRecycleViewFragment extends Fragment {

    RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;

    private Subscription subscription;
    public List<News> news = new ArrayList<>();
    private boolean isLoading;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_news_recycle_view, container, false);

        recyclerView = (RecyclerView)view.findViewById(R.id.newsRecycleView);

        getNews();

        final RecyclerViewAdapter adapter = new RecyclerViewAdapter(getContext(), news);
        adapter.setListener(new OnClickListener() {
            @Override
            public void onItemClick(News item) {
                Intent intent = new Intent(getActivity(), NewsActivity.class);
                intent.putExtra(NewsActivity.NEWS_ID, item.getId());
                getActivity().startActivity(intent);
            }
        });
        recyclerView.setAdapter(adapter);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
               NewsListService.resetObservable();
               getNews();
            }
        });

        return view;
    }

    private void getNews(){
        if (subscription != null && !subscription.isUnsubscribed()){
            subscription.unsubscribe();
        }

        NewsListService.setObservableRetrofit(NewsListService.getInstance().getJSONApi().getNewsByTag("Новости"));

        subscription = NewsListService.getObjectObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<News>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        isLoading = false;
                        swipeRefreshLayout.setRefreshing(false);
                    }

                    @Override
                    public void onNext(List<News> newNews) {
                        int prevSize = news.size();
                        isLoading = false;
                        if (isAdded()){
                            recyclerView.getAdapter().notifyItemRangeRemoved(0, prevSize);
                        }
                        news.clear();
                        news.addAll(newNews);
                        if (isAdded()){
                            recyclerView.getAdapter().notifyItemRangeInserted(0, news.size());
                        }
                        swipeRefreshLayout.setRefreshing(false);
                    }
                });
    }
}
