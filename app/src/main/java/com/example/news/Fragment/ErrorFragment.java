package com.example.news.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.news.R;


public class ErrorFragment extends Fragment {

    private String error;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_error, container, false);

        TextView errorText = (TextView)view.findViewById(R.id.errorText);
        errorText.setText(error);

        return view;
    }

    public void setError(String error) {
        this.error = error;
    }
}
