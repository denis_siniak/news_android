package com.example.news.Fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.news.Adapter.SectionsPageAdapter;
import com.example.news.DataBase.DataBaseHelper;
import com.example.news.R;


public class ViewPagerFragment extends Fragment {

    SectionsPageAdapter sectionsPageAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_pager, container, false);

        DataBaseHelper dataBaseHelper = new DataBaseHelper(getContext());

        sectionsPageAdapter = new SectionsPageAdapter(getActivity().getSupportFragmentManager(), dataBaseHelper.pullTags());
        ViewPager pager = (ViewPager)view.findViewById(R.id.viewPager);
        pager.setAdapter(sectionsPageAdapter);

        TabLayout tabLayout = (TabLayout)view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(pager);

        return view;
    }

    @Override
    public void onStop(){
        super.onStop();
        sectionsPageAdapter = null;
    }



}
