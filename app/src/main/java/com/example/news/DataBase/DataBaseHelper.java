package com.example.news.DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.news.Entity.NewsTag;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class DataBaseHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "savedTags";
    private static final int DB_VERSION = 1;

    public DataBaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        updateDatabase(db, 0, DB_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        updateDatabase(db, oldVersion, newVersion);
    }

    private void updateDatabase(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("CREATE TABLE SAVED_TAGS (_id INTEGER, TAG TEXT);");
    }

    public void insertTags(Set<NewsTag> tags){
        SQLiteDatabase db = this.getWritableDatabase();
        resetDB(db);
        for (NewsTag newsTag: tags){
            ContentValues values = new ContentValues();
            values.put("_id", newsTag.getId());
            values.put("TAG", newsTag.getName());
            db.insert("SAVED_TAGS", null, values);
        }
    }

    public List<NewsTag> pullTags(){
        SQLiteDatabase db = this.getReadableDatabase();
        List<NewsTag> newsTags = new ArrayList<>();
        newsTags.add(NewsTag.ALL);
        Cursor cursor = db.query("SAVED_TAGS", new String[] {"TAG"},null,null,null,null, null);

        while (cursor.moveToNext()){
            newsTags.add(NewsTag.findByName(cursor.getString(0)));
        }

        return newsTags;
    }

    private void resetDB(SQLiteDatabase db){
        db.execSQL("DROP TABLE SAVED_TAGS");
        db.execSQL("CREATE TABLE SAVED_TAGS (_id INTEGER, TAG TEXT);");
    }
}

